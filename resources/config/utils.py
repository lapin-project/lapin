# -*- coding: utf-8 -*-

import json

class Config:
    @classmethod
    def get(cls):
        with open('config.json') as f:
            config = json.load(f)
        return config

    @classmethod
    def set(cls, conf):
        with open('config.json', 'w') as f:
            f.write(json.dumps(conf))
