# -*- coding: utf-8 -*-

from flask_socketio import Namespace, emit
import PTN
from guessit import guessit
import tmdbsimple
from .utils import handle_error
from ..config.utils import Config


class FileParserNamespace(Namespace):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        tmdbsimple.API_KEY = Config.get()['tmdb']['key']
        self.search = tmdbsimple.Search()

        patterns = Config.get()['parser']['patterns']
        for k, p in patterns.items():
            pattern = next(pt for pt in PTN.patterns.patterns if pt[0] == k)
            del PTN.patterns.patterns[PTN.patterns.patterns.index(pattern)]
            PTN.patterns.patterns.append((k, p))

    def on_connect(self):
        pass

    def on_disconnect(self):
        pass

    @handle_error('/parser')
    def on_parse(self, data):
        ptnInfos = PTN.parse(data['filename'])
        infos = dict(guessit(data['filename']))
        if infos['type'] == 'episode':
            found = self.search.tv(
                query=infos['title'],
                language=Config.get()['tmdb']['language'])
        else:
            found = self.search.movie(
                query=infos['title'],
                language=Config.get()['tmdb']['language'])

        for k in ['subtitle_language', 'language']:
            if k in infos.keys():
                del infos[k]
        if 'language' in ptnInfos.keys():
            infos['language'] = ptnInfos['language']

        infos['results'] = found['results']

        for k, v in ptnInfos.items():
            if not k in infos.keys():
                infos[k] = v

        return {'filename': data['filename'], 'infos': infos}
