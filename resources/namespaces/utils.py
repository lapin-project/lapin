# -*- coding: utf-8 -*-

import traceback
from functools import wraps


def handle_error(namespace):
    def decorated(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                args[0].socketio.emit(
                    'error', {
                        'error': str(e),
                        'traceback': traceback.format_exc()
                    },
                    namespace=namespace)

        return wrapper

    return decorated
