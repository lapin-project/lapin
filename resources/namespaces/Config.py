# -*- coding: utf-8 -*-

from flask_socketio import Namespace, emit
from .utils import handle_error
from ..hosters.hosters import Hosters
from ..sources.sources import Sources
from ..config.utils import Config


class ConfigNamespace(Namespace):
    def on_connect(self):
        pass

    def on_disconnect(self):
        pass

    @handle_error('/config')
    def on_get_hosters(self):
        return [h.name for h in Hosters]

    @handle_error('/config')
    def on_get_sources(self):
        return [s.name for s in Sources]
