# -*- coding: utf-8 -*-

from flask_socketio import Namespace, emit
from .utils import handle_error
from ..sources.sources import Sources
from ..sources.exceptions import NoResultException, NoPageException


class SearchNamespace(Namespace):
    def _search(self, source_name, query):
        for source in Sources:
            if source.name == source_name:
                try:
                    return {'results': source.search(query)}
                except NoResultException:
                    return {'error': 'No results for ' + query}
        return {'error': 'No source named ' + source_name}

    def _get_links(self, source_name, url):
        for source in Sources:
            if source.name == source_name:
                try:
                    return {'links': source.get_links(url)}
                except NoPageException:
                    return {'error': 'Page at URL ' + url + 'does not exist'}
        return {'error': 'No source named ' + source_name}

    def on_connect(self):
        pass

    def on_disconnect(self):
        pass

    @handle_error('/search')
    def on_search(self, data):
        return self._search(data['source_name'], data['query'])

    @handle_error('/search')
    def on_get_links(self, data):
        return self._get_links(data['source_name'], data['url'])
