# -*- coding: utf-8 -*-

from flask_socketio import Namespace, emit
from ..core.Downloader import Downloader
from .utils import handle_error
from ..config.utils import Config


class DownloadNamespace(Namespace):
    def __init__(self,
                 *args,
                 threads=Config.get()['core']['threads'],
                 max_parallel=Config.get()['core']['max_parallel'],
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.downloader = Downloader(
            threads=threads, max_parallel=max_parallel)

        @self.downloader.progress.after
        def emit_progress(dl):
            self.socketio.emit(
                'progress',
                dl.as_dict(),
                namespace='/download',
                broadcast=True)

        @self.downloader.error.after
        def emit_error(dl):
            self.socketio.emit(
                'failed', dl.as_dict(), namespace='/download', broadcast=True)

        @self.downloader.success.after
        def emit_success(dl):
            self.socketio.emit(
                'success', dl.as_dict(), namespace='/download', broadcast=True)

    def on_connect(self):
        pass

    def on_disconnect(self):
        pass

    @handle_error('/download')
    def on_add(self, data):
        dl = self.downloader.add(data['url'], data['tv_show'],
                                 data['dest_folder'], data['dest_filename'],
                                 data['name'])
        emit('added', dl.as_dict(), broadcast=True)
        return dl.as_dict()

    @handle_error('/download')
    def on_cancel(self, data):
        self.downloader.cancel(data['uuid'])
        emit('canceled', {'uuid': data['uuid']}, broadcast=True)
        return {'uuid': data['uuid']}

    @handle_error('/download')
    def on_pause(self, data):
        self.downloader.pause(data['uuid'])
        emit('paused', {'uuid': data['uuid']}, broadcast=True)
        return {'uuid': data['uuid']}

    @handle_error('/download')
    def on_resume(self, data):
        self.downloader.resume(data['uuid'])
        emit('resumed', {'uuid': data['uuid']}, broadcast=True)
        return {'uuid': data['uuid']}

    @handle_error('/download')
    def on_get(self, data):
        return self.downloader.get(data['uuid']).as_dict()

    @handle_error('/download')
    def on_get_all(self):
        return [d.as_dict() for d in self.downloader.get_all()]

    @handle_error('/download')
    def on_get_infos(self, data):
        return {
            'url': data['url'],
            'infos': Downloader.get_file_infos(data['url'])
        }
