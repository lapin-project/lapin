#-*- coding: utf-8 -*-

import re
import requests
from ..config.utils import Config
from .exceptions import DeadLinkException, HosterException


class uptobox:
    name = 'Uptobox'
    link_regexp = 'https?://uptobox.com/[a-z0-9]+'

    def __init__(self):
        self._token = Config.get()['hosters']['uptobox']['key']

    def get_link(self, link):
        fileId = re.sub(r'(?is)https?://uptobox.com/', '', link)
        resp = requests.get('https://uptobox.com/api/link?token=' +
                            self._token + '&file_code=' + fileId)
        if resp.status_code == 200:
            if resp.json()['data'] == []:
                raise DeadLinkException()
            else:
                return resp.json()['data']['dlLink']
        else:
            raise HosterException('HTTP Error ' + resp.status_code)
