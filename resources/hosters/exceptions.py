# -*- coding: utf-8 -*-


class DeadLinkException(Exception):
    def __init__(self):
        super().__init__('Dead link')


class HosterException(Exception):
    pass
