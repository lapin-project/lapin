# -*- coding: utf-8 -*-

import importlib
from ..config.utils import Config

config = Config.get()

Hosters = []

for hoster in config['hosters'].keys():
    m = __import__('resources.hosters.' + hoster, fromlist=[hoster])
    c = getattr(m, hoster)
    Hosters.append(c())
