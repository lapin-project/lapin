# -*- coding: utf-8 -*-


class NoResultException(Exception):
    def __init__(self):
        super().__init__('No results')


class NoPageException(Exception):
    def __init__(self):
        super().__init__('Page not found')
