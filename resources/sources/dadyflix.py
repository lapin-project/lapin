# -*- coding: utf-8 -*-

import cfscrape
from bs4 import BeautifulSoup, SoupStrainer
from ..config.utils import Config
from .exceptions import NoResultException, NoPageException


class dadyflix:
    name = 'DadyFlix'

    def __init__(self):
        self._url = Config.get()['sources']['dadyflix']['url']
        self.session = cfscrape.create_scraper(delay=10)

    def _format_results(self, found):
        results = {}
        for f in found:
            if f['season'] in results.keys():
                results[f['season']][f['episode']] = f
            else:
                results[f['season']] = {f['episode']: f}
        return results

    def _get_infos(self, url):
        html = self.session.get(url, verify=False, timeout=15).text

        strainer = SoupStrainer('div', attrs={'class': 'poster'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        poster = soup.find('img')['src']

        strainer = SoupStrainer('div', attrs={'id': 'seasons'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        found = []

        for li in soup.findAll('li'):
            s = li.find('div', attrs={'class': 'numerando'}).text
            season = s.split('-')[0].strip()
            episode = s.split('-')[1].strip()
            found.append({
                'season': 'S{:02d}'.format(int(season)),
                'episode': 'E{:02d}'.format(int(episode)),
                'url': li.find('a')['href'],
                'poster': poster
            })

        return self._format_results(found)

    def search(self, query):
        html = self.session.get(
            self._url + '?s=' + query.replace(' ', '+'),
            verify=False,
            timeout=15).text

        strainer = SoupStrainer(
            'div', attrs={'class': 'content full_width_layout'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        infos = {}

        for div in soup.findAll('article'):
            infos[div.find('div', attrs={
                'class': 'title'
            }).find('a').text.strip()] = self._get_infos(
                div.find('a')['href'])

        return infos

    def get_links(self, url):
        html = self.session.get(url, verify=False, timeout=15).text

        strainer = SoupStrainer('div', attrs={'class': 'box_links'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        infos = {}

        for li in soup.findAll('li'):
            hoster = li.find('div').text.strip().capitalize()
            if hoster in infos.keys():
                i = 2
                while hoster + ' ' + str(i) in infos.keys():
                    i += 1
                infos[hoster + ' ' + str(i)] = {
                    'url': li.find('a')['data-src-player']
                }
            else:
                infos[hoster] = {'url': li.find('a')['data-src-player']}

        return infos
