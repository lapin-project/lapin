# -*- coding: utf-8 -*-

import cfscrape
from bs4 import BeautifulSoup, SoupStrainer
from ..config.utils import Config
from .exceptions import NoResultException, NoPageException


class zone_telechargement:
    name = 'Zone-Téléchargement'

    def __init__(self):
        self._url = Config.get()['sources']['zone_telechargement']['url']
        self.session = cfscrape.create_scraper(delay=10)
        self._data = {
            'do': 'search',
            'subaction': 'search',
            'search_start': '0',
            'full_search': '1',
            'result_from': '1',
            'all_word_seach': '1',
            'titleonly': '3',
            'searchuser': '',
            'replyless': '0',
            'replylimit': '0',
            'searchdate': '0',
            'beforeafter': 'after',
            'sortby': 'date',
            'resorder': 'desc',
            'showposts': '0',
            'catlist[]': '0'
        }
        self._hosters = {
            'uptobox.com': 'Uptobox',
            'ul.to': 'Uploaded',
            'turbobit.net': 'Turbobit',
            'nitroflare.com': 'NitroFlare',
            '1fichier.com': '1Fichier',
            'rapidgator.net': 'Rapidgator'
        }

    def _resolve_dl_protect(self, url):
        url = '/'.join(url.split('/')[3:])
        # Three character words
        words = [('062', ':'), ('063', '.'), ('067', '/')]
        for word in words:
            url = url.replace(word[0], word[1])

        # Two character words
        words = [('0f', '0'), ('0l', '1'), ('0r', '2'), ('0k', '3'),
                 ('0z', '4'), ('0x', '5'), ('0h', '6'), ('0o', '7'),
                 ('0m', '8'), ('0n', '9')]
        for i in range(0, 26):
            words.append((str(i + 36), chr(ord('a') + i)))

        i = 0
        while i < len(url) - 1:
            j = 0
            found = False
            while j < len(words) and not found:
                if words[j][0] in url[i:i + 2]:
                    url = url[:i] + url[i:i + 2].replace(
                        words[j][0], words[j][1]) + url[i + 2:]
                    found = True
                j += 1
            i += 1
        return url

    def _format_results(self, found):
        results = {}
        for f in found:
            if f['name'] in results.keys():
                if f['language'] in results[f['name']].keys():
                    results[f['name']][f['language']][f['quality']] = f
                else:
                    results[f['name']][f['language']] = {f['quality']: f}
            else:
                results[f['name']] = {f['language']: {f['quality']: f}}
        return results

    def _get_hoster(self, url):
        for hoster, name in self._hosters.items():
            if hoster in url:
                return name
        return 'Other'

    def search(self, query):
        self._data.update({'story': query})

        html = self.session.post(
            self._url, data=self._data, verify=False, timeout=15).text

        strainer = SoupStrainer('div', attrs={'id': 'dle-content'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        found = []

        for div in soup.findAll('div', attrs={'class': 'cover_global'}):
            found.append({
                'language':
                div.find('span', attrs={
                    'style': 'color:#ffad0a'
                }).text.strip().strip('()'),
                'quality':
                div.find('span', attrs={
                    'style': 'color:#1ba100'
                }).text.strip(),
                'url':
                div.find('a')['href'],
                'poster':
                div.find('img')['src']
            })

        # Get real titles
        self._data.update({'showposts': '1'})
        html = self.session.post(
            self._url, data=self._data, verify=False, timeout=15).text
        self._data.update({'showposts': '0'})

        strainer = SoupStrainer('div', attrs={'id': 'dle-content'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        h2s = soup.findAll('h2', attrs={'class': 'title'})
        for i in range(0, len(h2s)):
            found[i]['name'] = h2s[i].text.strip()

        return self._format_results(found)

    def get_links(self, url):
        r = self.session.get(url, verify=False, timeout=15)
        if r.status_code != 200:
            raise NoPageException
        html = r.text
        strainer = SoupStrainer('div', attrs={'class': 'postinfo'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        links = []
        for a in soup.findAll('a'):
            links.append({
                'name': a.text.strip(),
                'url': self._resolve_dl_protect(a['href'])
            })

        formatted = {}

        for link in links:
            hoster = self._get_hoster(link['url'])
            if hoster in formatted.keys():
                formatted[hoster][link['name']] = link
            else:
                formatted[hoster] = {link['name']: link}

        return formatted
