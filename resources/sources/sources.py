# -*- coding: utf-8 -*-

import importlib
from ..config.utils import Config

config = Config.get()

Sources = []

for source in config['sources'].keys():
    m = __import__('resources.sources.' + source, fromlist=[source])
    c = getattr(m, source)
    Sources.append(c())
