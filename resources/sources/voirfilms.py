# -*- coding: utf-8 -*-

import cfscrape
from bs4 import BeautifulSoup, SoupStrainer
from ..config.utils import Config
from .exceptions import NoResultException, NoPageException


class voirfilms:
    name = 'VoirFilms'

    def __init__(self):
        self._url = Config.get()['sources']['voirfilms']['url']
        self.session = cfscrape.create_scraper(delay=10)

    def _resolve_protect(self, url):
        html = self.session.get(url).text
        soup = BeautifulSoup(html, "lxml")

        return soup.find('a', attrs={'class': 'data download'})['href']

    def _get_link_list(self, soup):
        infos = {}

        for li in soup.findAll('li'):
            hoster = li.find(
                'span', attrs={
                    'class': 'gras'
                }).text.strip().capitalize()
            if hoster == '':
                hoster = li.find(
                    'span', attrs={
                        'class': 'gras'
                    }).find('img')['src'].split('/')[-1].split('.')[
                        0].strip().capitalize()
                url = self._resolve_protect(li.find('a')['href'])
            else:
                url = li.find('a')['data-src']
            if hoster in infos.keys():
                i = 2
                while hoster + ' ' + str(i) in infos.keys():
                    i += 1
                infos[hoster + ' ' + str(i)] = {'url': url}
            else:
                infos[hoster] = {'url': url}

        return infos

    def _get_infos(self, url):
        html = self.session.get(url, verify=False, timeout=15).text

        strainer = SoupStrainer('div', attrs={'id': 'content_all'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        soup = soup.find('div')
        img = soup.find('div', attrs={'class', 'infosaisons'}).find('img')
        if img is not None:
            poster = img['src']
        else:
            poster = 'https://httpbin.org/status/404'

        soup = soup.find('div', attrs={'info_film'})

        infos = {}

        if soup.find('div', attrs={'id': 'episodeliste'}) is not None:
            for li in soup.findAll('li'):
                infos[li.find('span').text.strip()] = {
                    'url': li.find('a')['href'],
                    'poster': poster
                }
            print(infos)
            return infos
        else:
            strainer = SoupStrainer('div', attrs={'class': 'link_list'})
            soup = BeautifulSoup(html, "lxml", parse_only=strainer)
            print('2')
            return self._get_link_list(soup)

    def search(self, query):
        html = self.session.get(
            self._url + 'recherche?story=' + query.replace(' ', '+'),
            verify=False,
            timeout=15).text

        strainer = SoupStrainer('div', attrs={'id': 'content_all'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        infos = {}

        for div in soup.findAll('div', attrs={'class': 'imagefilm'}):
            infos[div.find('div', attrs={
                'class': 'titreunfilm'
            }).text.strip()] = self._get_infos(div.find('a')['href'])

        return infos

    def get_links(self, url):
        html = self.session.get(url, verify=False, timeout=15).text

        strainer = SoupStrainer('div', attrs={'class': 'link_list'})
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        return self._get_link_list(soup)
