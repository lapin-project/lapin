# -*- coding: utf-8 -*-

import os.path
import time
import uuid
from threading import Thread
from pySmartDL import SmartDL
from .utils.debrider import debrid


class DownloadThread(Thread):
    def __init__(self, delay, download):
        super().__init__()
        self.download = download
        self.delay = delay
        self.running = False

    def run(self, *args, **kwargs):
        self.running = True
        i = 0
        while not self.download.isFinished() and self.running:
            time.sleep(0.1)
            i += 1
            if i % (self.delay *
                    10) == 0 and self.download.get_status() != 'paused':
                self.download.parent.progress(self.download)
        if self.download.isFinished():
            self.download.parent.finish(self.download)

    def stop(self):
        self.running = False


class Download(SmartDL):
    def __init__(self,
                 parent,
                 *args,
                 progress_delay=1,
                 dest_folder='',
                 dest_filename='',
                 name='',
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.thread = DownloadThread(progress_delay, self)
        self.parent = parent
        self.uuid = uuid.uuid4()
        self.destination = {'folder': dest_folder, 'filename': dest_filename}
        self.name = name

    def __del__(self):
        self.stop()

    def start(self, *args, **kwargs):
        super().start(*args, **kwargs, blocking=False)
        if not self.thread.is_alive():
            self.thread.start()

    def stop(self):
        if self.thread.is_alive():
            self.thread.stop()
            if not self.isFinished():
                self.thread.join()
        super().stop()

    def get_final_dest(self):
        return self.destination

    def as_dict(self):
        d = {
            'uuid':
            self.uuid.hex,
            'name':
            self.name,
            'destination':
            os.path.join(self.destination['folder'],
                         self.destination['filename']),
            'filesize':
            self.get_final_filesize(),
            'downloaded_size':
            self.get_dl_size(),
            'status':
            self.get_status(),
        }
        if self.get_status() != 'ready':
            d.update({
                'progress': self.get_progress(),
                'speed': self.get_speed(),
                'time': self.get_dl_time(),
                'eta': self.get_eta()
            })
        return d
