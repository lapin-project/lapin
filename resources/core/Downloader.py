# -*- coding: utf-8 -*-

import os
import pathlib
from urllib.parse import unquote
from functools import wraps
from pySmartDL import CanceledException, utils
from .Download import Download
from .utils.Hook import Hook
from .utils.Queue import Queue
from .utils.debrider import debrid
from ..config.utils import Config


class NoDownloadException(Exception):
    pass


class Downloader:
    def __init__(self, threads=1, max_parallel=5, progress_delay=1):
        self.queue = Queue()
        self.threads = threads
        self.max_parallel = max_parallel
        self.progress_delay = progress_delay

    def multitype(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if not isinstance(args[1], Download):
                args = list(args)
                args[1] = args[0].get(args[1])
                args = tuple(args)
            return f(*args, **kwargs)

        return wrapper

    def get(self, dl_uuid):
        dl = self.queue.find(condition=lambda x, y: x.uuid.hex == str(dl_uuid))
        if dl is None:
            raise NoDownloadException('No download found with UUID ' +
                                      str(dl_uuid))
        return dl

    def get_all(self):
        return self.queue.peek_all()

    def add(self, url, tv_show, dest_folder, dest_filename, name):
        if tv_show:
            dest_folder = Config.get()['files']['tv_shows_folder'] + dest_folder
        else:
            dest_folder += Config.get()['files']['movies_folder'] + dest_folder
        dl = Download(
            self,
            debrid(url),
            Config.get()['files']['downloads_folder'],
            progress_delay=1,
            dest_folder=dest_folder,
            dest_filename=dest_filename,
            name=name,
            progress_bar=False,
            threads=self.threads)
        self.queue.put(dl)
        self.start_next()
        return dl

    @multitype
    def cancel(self, dl):
        self.remove(dl)
        self.start_next()
        for i in range(0, self.threads):
            os.remove(dl.get_dest() + '.{:03d}'.format(i))

    @Hook
    @multitype
    def start(self, dl):
        dl.start()

    @Hook
    @multitype
    def progress(self, dl):
        pass

    @multitype
    def pause(self, dl):
        dl.pause()

    @multitype
    def resume(self, dl):
        dl.resume()

    @Hook
    @multitype
    def error(self, dl):
        pass

    @Hook
    @multitype
    def success(self, dl):
        pathlib.Path(dl.get_final_dest()['folder']).mkdir(
            parents=True, exist_ok=True)
        os.rename(
            dl.get_dest(),
            os.path.join(dl.get_final_dest()['folder'],
                         dl.get_final_dest()['filename']))

    @Hook
    @multitype
    def finish(self, dl):
        self.remove(dl)
        if dl.isSuccessful():
            self.success(dl)
        else:
            self.error(dl)
        self.start_next()

    @multitype
    def remove(self, dl):
        try:
            dl.stop()
        except CanceledException:
            pass
        self.queue.remove(dl)

    def start_next(self):
        for i in range(0, self.max_parallel):
            dl = self.queue.peek(i)
            if dl is None:
                break
            if dl.get_status() == 'ready':
                self.start(dl)

    @classmethod
    def get_file_infos(cls, url):
        url = debrid(url)
        return {
            'url': url,
            'filename': unquote(url).split('/')[-1],
            'filesize': utils.get_filesize(url)
        }
