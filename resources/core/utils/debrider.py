# -*- coding: utf-8 -*-

import re
from ...hosters.hosters import Hosters


class NoModuleException(Exception):
    pass


def select_module(link):
    for module in Hosters:
        if re.match(module.link_regexp, link):
            return module
    raise NoModuleException()


def debrid(link):
    module = select_module(link)
    debrided = module.get_link(link)
    return debrided
