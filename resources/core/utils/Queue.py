# -*- coding: utf-8 -*-


class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class Queue:
    def __init__(self):
        self.head = None

    def _get_node(self, index):
        prev = None
        node = self.head
        if node is None:
            return None, None
        for _ in range(0, index):
            prev = node
            node = node.next
            if node is None:
                return None, None
        if node is not None:
            return node, prev
        return None, None

    def _find_node(self, data=None, condition=None):
        i = 0
        if condition is not None:
            cond = condition
        else:
            cond = lambda x, y: x == y
        node = self.head
        if node is not None:
            if cond(node.data, data):
                return node, None, i
        while node is not None:
            if cond(node.data, data):
                break
            prev = node
            node = node.next
            i += 1
        if node is None:
            return None, None, -1
        return node, prev, i

    def put(self, data_in):
        new_node = Node(data_in)
        if self.head is None:
            self.head = new_node
            return
        last = self.head
        while last.next is not None:
            last = last.next
        last.next = new_node

    def get(self, index=0):
        node, prev = self._get_node(index)
        if prev is None:
            if node is not None:
                self.head = node.next
            else:
                return None
        else:
            prev.next = node.next
        data_out = node.data
        node = None
        return data_out

    def peek(self, index=0):
        node = self._get_node(index)[0]
        if node is not None:
            return node.data
        return None

    def peek_all(self):
        arr = []
        node = self.head
        while node is not None:
            arr.append(node.data)
            node = node.next
        return arr

    def remove(self, data):
        node, prev = self._find_node(data)[:2]
        if node is not None:
            if prev is not None:
                prev.next = node.next
            else:
                self.head = node.next
            node = None

    def find(self, data=None, condition=None):
        node = self._find_node(data, condition)[0]
        if node is not None:
            return node.data
        return None

    def empty(self):
        return self.head is None
