# Lapin core

## Setup

### Requirements

- Python >= 3.5.3
- Required packages : 
`pip install -r requirements`

### Configuration

Copy `config_example.json` in `config.json` and edit it so it fits your needs.

## Usage

`python3 server.py` will run the server according to config.json

## Disclaimer

Final users are the only responsibles for what they are downloading. All the developers of this project are no way responsible for how it is used.
