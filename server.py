# -*- coding: utf-8 -*-

from resources.config.utils import Config

if Config.get()['core']['async_mode'] == 'eventlet':
    if Config.get()['core']['monkey_patch']:
        import eventlet
        eventlet.monkey_patch()

from flask import Flask
from flask_socketio import SocketIO
from resources.namespaces.Download import DownloadNamespace
from resources.namespaces.FileParser import FileParserNamespace
from resources.namespaces.Search import SearchNamespace
from resources.namespaces.Config import ConfigNamespace

host = Config.get()['core']['host']
port = Config.get()['core']['port']

app = Flask(__name__)
socketio = SocketIO(
    app, async_mode=Config.get()['core']['async_mode'], async_handler=True)

socketio.on_namespace(DownloadNamespace('/download'))
socketio.on_namespace(FileParserNamespace('/parser'))
socketio.on_namespace(SearchNamespace('/search'))
socketio.on_namespace(ConfigNamespace('/config'))

if __name__ == '__main__':
    print('Running on ' + host + ':' + str(port))
    socketio.run(app, host=host, port=port)
